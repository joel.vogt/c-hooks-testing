#include <CUnit/Basic.h>

// mainly copied from https://myembeddeddiary.wordpress.com/2016/03/28/how-to-write-your-first-cunit-test-program-a-basic-example/

int init_suite(void) { return 0; }
int clean_suite(void) { return 0; }

void test_assert(){
    CU_ASSERT(1 == 1);
}

int main(){

    CU_pSuite pSuite1 = NULL;

    // Initialize CUnit test registry
    if (CUE_SUCCESS != CU_initialize_registry())
    return CU_get_error();

    // Add suite1 to registry
    pSuite1 = CU_add_suite("Basic_Test_Suite1", init_suite, clean_suite);
    if (NULL == pSuite1) {
    CU_cleanup_registry();
    return CU_get_error();
    }

    // add test1 to suite1
    if ((NULL == CU_add_test(pSuite1,  "\n\n……… Testing assert function……..\n\n", test_assert)))
    {
    CU_cleanup_registry();
    return CU_get_error();
    }

    CU_basic_run_tests();// OUTPUT to the screen

    CU_cleanup_registry();//Cleaning the Registry
    return CU_get_error();

}
